var request = require('request');

var url = 'http://localhost:3000/bitbucket';
var headers = {
  'x-event-key': 'repo:push'
};

var body = {  
  "repository":{  
    "uuid":"{74465174-7bce-466b-be64-a09cfbb30c68}",
    "name":"TestProject",
    "owner":{  
      "type":"user",
      "uuid":"{45c2c9fa-2674-4acd-8834-4d0f067e3109}",
      "links":{  
        "self":{  
          "href":"https://api.bitbucket.org/2.0/users/wubingru"
        },
        "avatar":{  
          "href":"https://bitbucket.org/account/wubingru/avatar/32/"
        },
        "html":{  
          "href":"https://bitbucket.org/wubingru/"
        }
      },
      "display_name":"wubingru",
      "username":"wubingru"
    },
    "is_private":false,
    "full_name":"wubingru/testproject",
    "type":"repository",
    "links":{  
      "self":{  
        "href":"https://api.bitbucket.org/2.0/repositories/wubingru/testproject"
      },
      "avatar":{  
        "href":"https://bitbucket.org/wubingru/testproject/avatar/32/"
      },
      "html":{  
        "href":"https://bitbucket.org/wubingru/testproject"
      }
    },
    "scm":"git"
  },
  "actor":{  
    "type":"user",
    "uuid":"{45c2c9fa-2674-4acd-8834-4d0f067e3109}",
    "links":{  
      "self":{  
        "href":"https://api.bitbucket.org/2.0/users/wubingru"
      },
      "avatar":{  
        "href":"https://bitbucket.org/account/wubingru/avatar/32/"
      },
      "html":{  
        "href":"https://bitbucket.org/wubingru/"
      }
    },
    "display_name":"wubingru",
    "username":"wubingru"
  },
  "push":{  
    "changes":[  
      {  
        "closed":false,
        "old":null,
        "created":true,
        "new":{  
          "type":"branch",
          "target":{  
            "message":"web hook test\n",
            "author":{  
              "raw":"ananwbr <benraywu@gmail.com>",
              "user":{  
                "type":"user",
                "uuid":"{45c2c9fa-2674-4acd-8834-4d0f067e3109}",
                "links":{  
                  "self":{  
                    "href":"https://api.bitbucket.org/2.0/users/wubingru"
                  },
                  "avatar":{  
                    "href":"https://bitbucket.org/account/wubingru/avatar/32/"
                  },
                  "html":{  
                    "href":"https://bitbucket.org/wubingru/"
                  }
                },
                "display_name":"wubingru",
                "username":"wubingru"
              }
            },
            "parents":[  

            ],
            "hash":"d1414d6ec3b15f247ac76cfb07317361d346b639",
            "date":"2015-11-25T05:03:55+00:00",
            "type":"commit",
            "links":{  
              "self":{  
                "href":"https://api.bitbucket.org/2.0/repositories/wubingru/testproject/commit/d1414d6ec3b15f247ac76cfb07317361d346b639"
              },
              "html":{  
                "href":"https://bitbucket.org/wubingru/testproject/commits/d1414d6ec3b15f247ac76cfb07317361d346b639"
              }
            }
          },
          "links":{  
            "self":{  
              "href":"https://api.bitbucket.org/2.0/repositories/wubingru/testproject/refs/branches/master"
            },
            "commits":{  
              "href":"https://api.bitbucket.org/2.0/repositories/wubingru/testproject/commits/master"
            },
            "html":{  
              "href":"https://bitbucket.org/wubingru/testproject/branch/master"
            }
          },
          "name":"master"
        },
        "truncated":false,
        "commits":[  
          {  
            "message":"web hook test\n",
            "author":{  
              "raw":"ananwbr <benraywu@gmail.com>",
              "user":{  
                "type":"user",
                "uuid":"{45c2c9fa-2674-4acd-8834-4d0f067e3109}",
                "links":{  
                  "self":{  
                    "href":"https://api.bitbucket.org/2.0/users/wubingru"
                  },
                  "avatar":{  
                    "href":"https://bitbucket.org/account/wubingru/avatar/32/"
                  },
                  "html":{  
                    "href":"https://bitbucket.org/wubingru/"
                  }
                },
                "display_name":"wubingru",
                "username":"wubingru"
              }
            },
            "links":{  
              "self":{  
                "href":"https://api.bitbucket.org/2.0/repositories/wubingru/testproject/commit/d1414d6ec3b15f247ac76cfb07317361d346b639"
              },
              "html":{  
                "href":"https://bitbucket.org/wubingru/testproject/commits/d1414d6ec3b15f247ac76cfb07317361d346b639"
              }
            },
            "hash":"d1414d6ec3b15f247ac76cfb07317361d346b639",
            "type":"commit"
          }
        ],
        "links":{  
          "commits":{  
            "href":"https://api.bitbucket.org/2.0/repositories/wubingru/testproject/commits?include=d1414d6ec3b15f247ac76cfb07317361d346b639"
          },
          "html":{  
            "href":"https://bitbucket.org/wubingru/testproject/branch/master"
          }
        },
        "forced":false
      }
    ]
  }
}
;

var options = {
  url: url,
  headers: headers,
  json: true,
  body: body
};

function callback(error, response, body) {
  if (!error && response.statusCode == 200 ) {
    console.log(body);
  }
}

request.post(options, callback);
